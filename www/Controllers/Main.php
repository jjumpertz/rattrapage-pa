<?php
//Parse error: syntax error, unexpected 'Global' (T_GLOBAL), expecting identifier (T_STRING) in /var/www/html/Controllers/Global.php on line 3

namespace App\Controllers;

use App\Core\Exception\UnauthorizedException;
use App\Core\View;
use App\Core\Database;
use App\Middleware\AuthorizationMiddleware;
use App\Models\Categories as modelCategories;
use App\Models\Comment as CommentModel;
use App\Models\User as UserModel;
use App\Models\Page as PageModel;


class Main
{

    /* Lister les 5 derniers commentaires postés sur la page d'accueil */
    public function defaultAction()
    {

        $pseudo = ""; // Depuis la bdd

        $pages = (new PageModel())->getWhereAnd(["status" => "> 0"], false);

        //Affiche la vue home intégrée dans le template du front
        $view = new View("home", "front");
        if (isset($_SESSION['role'])) {
            $pseudo = $_SESSION['pseudo'];
            $view->setTemplate("front");
        }

        $view->assign("pages", $pages);
		$view->assign("pseudo", $pseudo);

    }//defaultAction

    /* Afficher la page 404 Not Found */
    public static function page404Action()
    {
        //Affiche la vue 404 intégrée dans le template du front
        $view = new View("404");
    }//page404Action

    /* Afficher la page 401 Unauthorized */
    public static function page401Action()
    {
        $view = new View("401");
    }//page401Action

    /* Afficher la page 403 Forbidden */
    public static function page403Action()
    {
        $view = new View("403");
    }//page403Action

    public static function page553Action()
    {
        $view = new View("Errors/errorLogin");
    }//page553Action

    /* Afficher le tableau de bord */
    public function showDashboardAction()
    {
        if (isset($_SESSION['id']) && $_SESSION['id'] != "") {
            $comments = new CommentModel();

            foreach ($comments as $key => $value) {
                $user = (new UserModel())->getOneById($comments[$key]['user']);
                $comments[$key]['user'] = $user[0]['pseudo'];
            }
            $view = new view("home");
            $category = new modelCategories();
            $form = $category->formBuilderCategory();
            $categories = $category->getAllData();
            $view->assign("categories", $categories);
            $view->assign("form", $form);
            $view->assign("comments", $comments);
        }else {
            $view = new View("Errors/errorLogin", "front");
        }
    }//showDashboardAction

}