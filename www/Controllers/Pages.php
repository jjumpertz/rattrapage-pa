<?php
namespace App\Controllers;

use App\Core\Exception\NotFoundException;
use App\Core\Exception\NotLoggedInException;
use App\Core\FormValidator;
use App\Core\Router;
use App\Core\View;
use App\Middleware\AuthorizationMiddleware;
use App\Models\Page;
use App\Models\Page as PageModel;


class Pages{


    /* Lister toutes les pages créées */
	public function defaultAction(){
        if (isset($_SESSION['id']) && $_SESSION['id'] != "") {
            $form = PageModel::formBuilderAdd();

            $status = ["Brouillon", "Publiée"];

            $view = new View("pages");
            //$view->setTemplate("back");

            $examplePage = new PageModel();
            $pages = $examplePage->showPages();

            foreach ($pages as $key => $value) {
                $user = (new \App\Models\User())->getOneById($pages[$key]->getUser());
                $pages[$key]->setUser($user->getPseudo());
                $pages[$key]->setStatus($status[$pages[$key]->getStatus()]);
            }

            if (isset($_COOKIE['errors'])) {
                $view->assign("messages", unserialize($_COOKIE['errors']));
                setcookie("errors", "", time() - 3600);
            }
            if (isset($_COOKIE['success'])) {
                $view->assign("messages", unserialize($_COOKIE['success']));
                setcookie("success", "", time() - 3600);
            }

            $view->assign("formAddPage", $form);
            $view->assign("pages", $pages);
        } else {
            new NotLoggedInException();
        }
    }//defaultAction


    /* Créer une page */
	public function addAction(){
        unset($_COOKIE['errors']);
        if (isset($_SESSION['id']) && $_SESSION['id'] != "") {

            $errors = FormValidator::check(PageModel::formBuilderAdd(), $_POST);

            if (empty($errors)) {
                $newURL = trim($_POST['url']);

                if ($newURL === "") {
                    $newURL = preg_replace("/[^\w\s]/", "", $_POST['title']);
                    if (empty(trim($newURL))) {
                        $errors[] = "Titre de la page incorrect. Si vous voulez utiliser ce nom de page, veuillez renseigner l'url";
                    } else {
                        $newURL = "/" . preg_replace("/[\s]/", "-", $newURL);
                    }
                } else {
                    if (preg_match("/\/[A-Za-z][A-Za-z0-9\-\#\/]*/", $newURL, $matches, PREG_UNMATCHED_AS_NULL)) {
                        $newURL = $matches[0];
                    } else {
                        $errors[] = "URL non conforme";
                    }
                }
                $urlExists = (new PageModel())->getWhereAnd(["lien" => "= '" . $newURL . "'"]) ?? false;
                $titleExists = (new PageModel())->getWhereAnd(["title" => "= '" . $_POST['title'] . "'"]) ?? false;

                if ($urlExists) {
                    $errors[] = "URL déjà utilisée";
                }
                if ($titleExists) {
                    $errors[] = "Titre déjà utilisé";
                }

                if (empty($errors)) {
                    $newPage = new PageModel();

                    $newPage->setLien($newURL);
                    $newPage->setTitle($_POST['title']);
                    $newPage->setContent("<h2>" . $_POST['title'] . "</h2>");
                    $newPage->setUser($_SESSION['id']);

                    $newPage->save();

                    $success[] = "Page ajoutée avec succès !";
                    setcookie("success", serialize($success));
                    header('Location: ' . Router::getRoute("Pages", "default"));

                }
            }
        } else {
            new NotLoggedInException();
        }
        setcookie("errors", serialize($errors));
        header('Location: ' . Router::getRoute("Pages", "default"));

    }//addAction

    /* Modifier un page */
	public function editAction(){
        if (isset($_SESSION['id']) && $_SESSION['id'] != "") {
            $view = new View("editor");
            //$view->setTemplate("back");

            if (isset($_POST['id']) && is_numeric($_POST['id'])) {
                $pageById = new PageModel();
                $page = $pageById->getOneById();
                $view->assign("page", $page);
            }
        } else {
            new NotLoggedInException();
        }

    }//editAction

    public function publierAction() {
        if (isset($_SESSION['id']) && $_SESSION['id'] != "") {
            if (isset($_POST['id'])) {
                $page = new PageModel();
                $page->setId($_POST['id']);
                $page->setStatus(1);
                $page->save();
                echo json_encode(["reponse" => 200, "msg" => "Page publiée"]);
                return;
            }
            echo json_encode(["reponse" => 400, "msg" => "Erreur"]);
        } else {
            new NotLoggedInException();
        }
    }

    /* Sauvegarder les modifications apportées à la page */
    public function saveAction() {
        if (isset($_SESSION['id']) && $_SESSION['id'] != "") {
            if (isset($_POST['id'])) {
                $newURL = trim($_POST["url"]);
                if ($newURL != "" && preg_match("/\/[A-Za-z][A-Za-z0-9\-\#\/]*/", $newURL, $matches, PREG_UNMATCHED_AS_NULL)) {
                    $urlExists = (new PageModel())->getWhereAnd(["lien" => "= '" . $newURL . "'"]) ?? false;
                    $titleExists = (new PageModel())->getWhereAnd(["title" => "= '" . $_POST['title'] . "'"]) ?? false;
                    if ($urlExists && $urlExists->getId() != $_POST['id']) {
                        echo json_encode(["reponse" => 400, "msg" => "URL déjà prise"]);
                        return;
                    }
                    if ($titleExists && $titleExists->getId() != $_POST['id']) {
                        echo json_encode(["reponse" => 400, "msg" => "Titre déjà pris"]);
                        return;
                    }
                    $pageSave = new PageModel();
                    $pageSave->setId($_POST['id']);
                    $pageSave->setUser($_SESSION['id']);
                    $pageSave->setTitle($_POST['title']);
                    $pageSave->setLien($_POST['url']);
                    $pageSave->setContent($_POST['content']);
                    $pageSave->setStatus(0);

                    $pageSave->save();
                    echo json_encode(["reponse" => 200, "msg" => "Sauvegarde effectuée !"]);
                    return;
                }
                echo json_encode(["reponse" => 400, "msg" => "Veuillez saisir une URL valide"]);
                return;
            }
            echo json_encode(["reponse" => 400, "msg" => "Erreur"]);
            return;
        } else {
            new NotLoggedInException();
        }

    }//saveAction

    /* Supprimer une page */
	public function deleteAction() {
        if (isset($_SESSION['id']) && $_SESSION['id'] != "") {

            if (isset($_POST['id']) && is_numeric($_POST['id'])) {
                $pageById = new PageModel();
                $pageById->remove();

                header('Location: ' . Router::getRoute("Pages", "default"));
            }
        } else {
            new NotLoggedInException();
        }
    }//deleteAction

    /* Afficher le contenu d'une page */
    public function showAction() {
        if (isset($_GET['id'])) {
            $page = (new PageModel())->getOneById();
        } else if (isset($_GET['url'])){
            $page = (new PageModel())->getWhereAnd(["lien" => "= '" . $_GET['url'] . "'"]) ?? false;
        } else {
            new NotFoundException();
        }
        if (!empty($page)) {
            $view = new View("display");
            $createur = (new \App\Models\User())->getOneById($page->getUser());
            $view->assign("page", $page);
            $view->assign("createur", $createur);
        } else {
            new NotFoundException();
        }
    }//showAction
}