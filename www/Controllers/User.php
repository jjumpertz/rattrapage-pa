<?php

namespace App\Controllers;

use App\Core\Exception\NotFoundException;
use App\Core\Exception\NotLoggedInException;
use App\Core\Mails;
use App\Core\Router;
use App\Core\View;
use App\Core\FormValidator;
use App\Models\User as UserModel;
use PDO;

class User
{

    public function __construct()
    {

    }

    /* Se créer un compte sur le site */
    public function registerAction()
    {
        $user = new userModel();
        $view = new View("register");

        $form = $user->formbuilderRegister();
        $view->assign("form", $form);

        if (!empty($_POST)) {
            $errors = FormValidator::check($form, $_POST);
            if (empty($errors)) {
                //if pseudo available
                if ($user->getOneBy("pseudo", $_POST['pseudo']))
                    $errors[] = 'Ce pseudo est déjà utilisé';
                //if email available
                if ($user->getOneBy("email", $_POST['email']))
                    $errors[] = 'Cette adresse mail est déjà utilisé';

                if ($_POST['pwd'] != $_POST['pwdConfirm'])
                    $errors[] = 'Les mots de passe ne concordent pas';

                if (empty($errors)) {
                    $user->setPseudo($_POST["pseudo"]);
                    $user->setEmail($_POST["email"]);
                    $user->setPwd(password_hash($_POST["pwd"], PASSWORD_BCRYPT));
                    $user->generateKeyEmail();
                    $user->save();


                    //Send Email
                    $userId = $user->lastIDInserted();
                    $to = $_POST['email'];
                    $from = MAIL_USER;
                    $name = SITE_NAME;
                    $subj = "Confirmez votre Email.";
                    $msg = "http://localhost:8080".Router::getRoute("User", "verifEmail")."?id=".$userId."&key=".$user->getEmailKey();
                    $mail = new Mails();
                    $mail->sendMail($to, $from, $name, $subj, $msg);
                }
            }
            $view->assign("errors", $errors);
        }
    }//registerAction

    /* Se connecter au site avec un compte existant */
    public function loginAction()
    {
        $view = new View("login", "front");

        $user = new UserModel();
        $form = $user->formBuilderLogin();

        if (!empty($_POST)) {

            $errors = FormValidator::check($form, $_POST);

            if (empty($errors)) {
                $user->setEmail($_POST["email"]);
                $user->setPwd($_POST["pwd"]);

                if ($user->login()) {
                    header('Location: ' . Router::getRoute("Main", "default"));
                } else {
                    $errors[] = "Email / Mot de passe incorrect(s) ou Email non confirmé. Veuillez réessayez.";
                    $view->assign("errors", $errors);
                }
            } else {
                $view->assign("errors", $errors);
            }
        }

        $view->assign("formLogin", $form);
    }//loginAction

    /* Afficher les informations relatives au compte */
    public function showAction()
    {
        if (isset($_SESSION['id']) && $_SESSION['id'] != "") {
            $view = new View("users/user", "front");
            $UserById = new userModel();
            $user = $UserById->getOneByIdSession();
            //Affiche la vue user intégrée dans le template du front
            if (isset($_POST['id']) && is_numeric($_POST['id'])) {
                //$view = new View("user", "front");
                $UserById = new userModel();
                $user = $UserById->getOneById();
                if (!empty($user) && $user->getIsDeleted() == 0) $view->assign("user", $user);
                else new NotFoundException();
            } else if ($user->getId()) {
                $view->assign("user", $user);
            }
        } else {
            new NotLoggedInException();
        }
    }//showAction

    /* Lister tous les comptes du site */
    public function showAllAction()
    {

        if (isset($_SESSION['id']) && $_SESSION['id'] != "") {
            //Affiche la vue users intégrée dans le template du back
            $view = new View("users/users", "front");
            $oneUser = new userModel();
            $users = $oneUser->getWhereAnd(["isDeleted" => "= 0"], false);
            $view->assign("users", $users);
        } else {
            echo "vous n'avez pas les droits, veuillez vous connecter";
            $this->loginAction();
        }
        /*$view = new View('users','back');
        $users = $this->user->showUsers();
        $view->assign('users', $users);*/
    }//showAllAction

    /* Sauvegarde les modifications apportées au compte */
    public function updateUserAction()
    {
        if (isset($_SESSION['id']) && $_SESSION['id'] != "") {
            $user = new userModel();
            //$id = $user->getId();

            $view = new View("users/userUpdate", "front");
            $form = $user->formBuilderUpdate();
            $formPwd = $user->formBuilderUpdatePassword();

            if (isset($_COOKIE['errors'])) {
                $view->assign("messages", unserialize($_COOKIE['errors']));
                setcookie("errors", "", time() - 3600);
            }
            if (isset($_COOKIE['success'])) {
                $view->assign("messages", unserialize($_COOKIE['success']));
                setcookie("success", "", time() - 3600);
            }

            $view->assign("formUpdateProfile", $form);
            $view->assign("formUpdatePwd", $formPwd);
        } else {
            new NotLoggedInException();
        }
    }

    /* Mettre à jour les informations du compte */
    public function updateProfileAction() {
        unset($_COOKIE['errors']);
        if (isset($_SESSION['id']) && $_SESSION['id'] != "") {
            if (!empty($_POST)) {
                $user = new UserModel();
                $errors = FormValidator::check($user->formBuilderUpdate(), $_POST);
                if (empty($errors)) {
                    $userFind = $user->getWhereAnd(["email" => "= '" . $_POST['email'] . "'"]) ?? false;
                    if (!$userFind || $userFind->getEmail() == $_SESSION['email']) {
                        $user->setId($_SESSION['id']);
                        $user->setPseudo($_POST["pseudo"]);
                        $user->setEmail($_POST["email"]);
                        $user->save();

                        $success[] = "Information du compte modifiées avec succès !";
                        setcookie("success", serialize($success));
                        header('Location: ' . Router::getRoute("User", "updateUser"));
                    } else {
                        $errors[] = "Adresse Email déjà utilisée ! ";
                    }
                }
            }
        } else {
            new NotLoggedInException();
        }
        setcookie("errors", serialize($errors));
        header('Location: ' . Router::getRoute("User", "updateUser"));
    }

    /* Mettre à jour le mot de passe du compte */
    public function updatePasswordAction() {
        unset($_COOKIE['errors']);
        if (isset($_SESSION['id']) && $_SESSION['id'] != "") {
            if (!empty($_POST)) {
                $user = new UserModel();
                $errors = FormValidator::check($user->formBuilderUpdatePassword(), $_POST);
                if (empty($errors)) {
                    if ($_POST['pwd'] === $_POST['pwdConfirm']) {
                        $user->setId($_SESSION['id']);
                        $user->setPwd(password_hash($_POST["pwd"], PASSWORD_BCRYPT));
                        $user->save();

                        $success[] = "Mot de passe modifié avec succès !";
                        setcookie("success", serialize($success));
                        header('Location: ' . Router::getRoute("User", "updateUser"));
                    } else {
                        $errors[] = "Les mots de passe ne concordent pas ! ";
                    }
                }
            }
        } else {
            new NotLoggedInException();
        }
        setcookie("errors", serialize($errors));
        header('Location: ' . Router::getRoute("User", "updateUser"));
    }


    /* Supprimer un utilisateur */
    public function removeUserAction()
    {
        if (isset($_SESSION['id']) && $_SESSION != "") {
            $id = $_POST['id'];

            if (isset($id)) {
                $removeUser = (new userModel())->getOneById();
                $removeUser->setIsDeleted(1);
                $removeUser->save();
                $_SESSION['alert']['success'][] = "L'utilisateur a bien été supprimé.";
                if ($id == $_SESSION['id']) {
                    session_destroy();
                    header('Location: ' . Router::getRoute("User", "login"));
                }
            } else {
                $_SESSION['alert']['danger'][] = "Une erreur s'est produite.";
            }
            header('Location: ' . Router::getRoute("User", "showAll"));
        } else {
            new NotLoggedInException();
        }
    }//removeUserAction

    /* Se déconnecter du site */
    public function deconnexionAction()
    {
        session_destroy();
        header('Location: ' . Router::getRoute("User", "login"));
    }//deconnexionAction


    /* Réinitialisation du mot de passe et envoie du nouveau  */
    /* mot de passe à l'adresse mail du compte */
    public function resetPasswordAction()
    {
        $view = new View("resetPwd");
        $user = new UserModel();

        $form = $user->formBuilderResetPassword();

        if (!empty($_POST['email'])) {

            $errors = FormValidator::check($form, $_POST);

            if (empty($errors)) {
                $userFind = $user->getWhereAnd(["email" => "= '" . $_POST['email'] . "'", "isDeleted" => "= 0"]) ?? false;

                if ($userFind) {
                    $newPwd = uniqid();
                    $user->setId($userFind->getId());
                    $user->setPwd(password_hash($newPwd, PASSWORD_BCRYPT));

                    $to = $_POST['email'];
                    $from = MAIL_USER;
                    $name = SITE_NAME;
                    $subj = SITE_NAME . " - Réinitialisation du mot de passe";
                    $msg = "<html><head></head><body>Bonjour <b><u>" . $userFind->getPseudo() . "</u></b>, <br>
                    Votre mot de passe a bien été réinitialisé. <br> Votre nouveau mot de passe : <b>" . $newPwd . "</b></body></html>";
                    $alt = "Bonjour " . $userFind->getPseudo() . ",
                    Votre mot de passe a bien été réinitialisé. Votre nouveau mot de passe : " . $newPwd;

                    $mailReset = new Mails();
                    $mailReset->sendMail($to, $from, $name, $subj, $msg, $alt);

                    //$mailReset->send();
                    $user->save();
                } else {
                    $errors[] = "Compte inexisant ! ";
                    $view->assign("errors", $errors);
                }

            } else {
                $view->assign("errors", $errors);
            }

        }

        $view->assign("form", $form);
    }

    public function verifEmailAction(){
        $view = new View("userVerifEmail");

        if(isset($_GET['id']) && !empty($_GET['id']) && isset($_GET['key']) && !empty($_GET['key'])){

            $user = (new UserModel())->getWhereAnd(["id" => "= '".$_GET['id']."'"]);

            $verifId = $user->getId();
            $verifKey = $user->getEmailKey();

            if ($_GET['id'] == $verifId && $_GET['key'] == $verifKey){
                if ($user->isEmailConfirmed() != true){
                    $user->setEmailConfirmed(1);

                    $user->save();
                    header('Location: ' . Router::getRoute("User", "login"));
                }

            }

        }else{
            echo "Aucun utilisateur trouvé";
        }
    }
}
