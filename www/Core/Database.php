<?php
namespace App\Core;

use App\Models\User;
use PDO;

class Database{

    private $pdo;
    private $table;

    /* Création de la connexion à la base de données */
    public function __construct(){
        try{
            $this->pdo = new PDO( DB_DRIVER.":host=".DB_HOST.";dbname=".DB_NAME.";port=".DB_PORT , DB_USER , DB_PWD );
        }catch(\PDOException $e){
            echo "Erreur de connexion à la BDD";
        }
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sql = "SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));";
        $query = $this->pdo->prepare($sql);
        $query->execute();

        //  jclm_   App\Models\User -> jclm_User
        $classExploded = explode("\\", get_called_class());
        $this->table = strtolower(DB_PREFIXE.end($classExploded)); //jclm_User
    }//__construct

    /* Sauvegarde ou mise à jour d'une ligne dans la base de données */
    public function save(){

        $column = array_diff_key(get_object_vars($this), get_class_vars(get_class()));
        $column = array_filter($column, "strlen");
        if( is_null($this->getId()) ){
            //INSERT
            $query = $this->pdo->prepare("INSERT INTO ".$this->table." 
						(".implode(',', array_keys($column)).") 
						VALUES 
						(:".implode(',:', array_keys($column)).") "); //1
        } else {
            //UPDATE
            $sqlSet = self::sqlSet($column);
            $sql = "UPDATE " . $this->table . " SET " . implode(",", $sqlSet) . " WHERE id=" . $this->getId();
            $query = $this->pdo->prepare($sql);
        }
        $query->execute($column);
    }//save

    /* Vérification de l'existence du compte et si */
    /* le mot de passe correspond à celui passé en paramètre */
    public function login() {
        $column = array_diff_key(
            get_object_vars($this)
            ,
            get_class_vars(get_class())
        );

        $query = $this->pdo->prepare("SELECT `id`, `pwd`, `pseudo`, `role`, `email` FROM ".$this->table." WHERE email = :email and isDeleted = 0 and emailConfirmed = 1");
        $query->execute(["email" => $_POST["email"]]);
        $reponse = $query->fetch();

        if ($query->rowCount() > 0) {
            if (password_verify($_POST['pwd'], $reponse['pwd'])) {
                session_start();
                $_SESSION['id'] = $reponse['id'];
                $_SESSION['email'] = $reponse['email'];
                $_SESSION['pseudo'] = $reponse['pseudo'];
                $_SESSION['role'] = $reponse['role'];
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }//login

    /* Préparer les colonnes à mettre à jour */
    private function sqlSet($where)
    {
        foreach ($where as $key => $value) {
            $sqlWhere[] = $key . "=:" . $key;
        }
        return $sqlWhere;
    }//sqlSet

    /* Préparer les conditions de récupération de données */
    private function sqlWhere($where)
    {
        foreach ($where as $key => $value) {
            $sqlWhere[] = $key . " " . $value;
        }
        return $sqlWhere;
    }//sqlWhere

    /* Récupérer toutes les informations d'une table */
    public function getAllData()
    {
        $sql = "SELECT * FROM ". $this->table;
        $query = $this->pdo->prepare($sql);
        $query->setFetchMode(PDO::FETCH_CLASS, get_class($this));
        $query->execute();
        return $query->fetchAll();
    }//getAllData

    /* Récupérer toutes les informations relatives à un identifiant */
    public function getOneById($id = null){
        if ($id != null) {
            $sql = "SELECT * FROM ". $this->table . " WHERE id = " . $id;
        } else {
            $sql = "SELECT * FROM ". $this->table . " WHERE id = " . ($_GET['id'] ?? $_POST['id']);
        }
        $query = $this->pdo->prepare($sql);
        $query->setFetchMode(PDO::FETCH_CLASS, get_class($this));
        $query->execute();
        return $query->fetch();
    }//getOneById

    public function getOneBy($column, $value){
        $query = $this->pdo->query('SELECT * FROM '.$this->table.' WHERE `'.$column. '` =  "' .htmlspecialchars($value). '"');
        $query->setFetchMode(\PDO::FETCH_CLASS, get_class($this));
        return $query->fetch();
    }

    /* Supprimer une ligne en renseignant un identifiant */
    public function remove()
    {
        $id = $_POST['id'];
        if($id){
            $sql = "DELETE FROM " . $this->table . " WHERE id=".$id.";";
            $query = $this->pdo->prepare($sql);
            $query->execute();
        }
    }//remove

    /* Récupérer toutes les informations relatives à une ou plusieurs conditions */
    public function getWhereAnd(array $where, bool $first = true)
    {
        $sqlWhere = $this->sqlWhere($where);
        $sql = "SELECT * FROM " . $this->table . " WHERE " . implode(" AND ", $sqlWhere) . ";";
        $query = $this->pdo->prepare($sql);
        $query->setFetchMode(PDO::FETCH_CLASS, get_class($this));
        $query->execute();
        if ($first) return $query->fetch();
        return $query->fetchAll();
    }//getWhereAnd

    /* Récupérer toutes les informations relatives à l'identifiant */
    /* renseigné en session */
    public function getOneByIdSession(){
        $sql = "SELECT * FROM ". $this->table . " WHERE id = " . $_SESSION['id'];
        $query = $this->pdo->prepare($sql);
        $query->setFetchMode(PDO::FETCH_CLASS, get_class($this));
        $query->execute();
        return $query->fetch();
    }//getOneByIdSession

    /* Récupérer les 5 dernières lignes maximum d'une table */
    public function getFiveLast() {
        $sql = "SELECT * FROM " . $this->table . " GROUP BY created_at ORDER BY created_at DESC LIMIT 5;";
        $query = $this->pdo->prepare($sql);
        $query->setFetchMode(PDO::FETCH_CLASS, get_class($this));
        $query->execute();
        return $query->fetchAll();
    }//getFiveLast

    public function lastIDInserted(): string
    {
        return $this->pdo->lastInsertId();

    }
}
