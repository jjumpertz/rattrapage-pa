<?php

namespace App\Core\Exception;

use App\Controllers\Main;
use App\Core\Router;
use App\Core\View;
use Throwable;

class ExceptionHandler extends \Exception implements \Throwable {
    protected $message;
    protected $code;

    /* Redirection vers la page adéquate selon l'exception lancée */
    public function __construct($message = "", $code = 0)
    {
        parent::__construct($message, $code);
        if ($code == 404) {
            header('Location: '. Router::getRoute('Main', 'page404'));
        }
        elseif ($code == 401) {
            header('Location: '. Router::getRoute('Main', 'page401'));
        }
        elseif ($code == 403) {
            header('Location: '. Router::getRoute('Main', 'page403'));
        }
        elseif ($code == 553) {
            header('Location: '. Router::getRoute('Main', 'page553'));
        }
    }
}
