<?php

namespace App\Core\Exception;

class ForbiddenException extends ExceptionHandler {
    /* Construction de l'exception d'accès interdit */
    public function __construct($message = "Accès interdit", $code = 403)
    {
        parent::__construct($message, $code);
    }
}
