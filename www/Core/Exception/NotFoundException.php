<?php

namespace App\Core\Exception;

class NotFoundException extends ExceptionHandler {
    /* Construction de l'exception de page non trouvée */
    public function __construct($message = "Page non trouvée", $code = 404)
    {
        parent::__construct($message, $code);
    }
}
