<?php

namespace App\Core\Exception;

class NotLoggedInException extends ExceptionHandler {
    /* Construction de l'exception d'accès non autorisé */
    public function __construct($message = "Vous devez être connecter", $code = 553)
    {
        parent::__construct($message, $code);
    }
}
