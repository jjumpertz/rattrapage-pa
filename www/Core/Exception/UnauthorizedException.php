<?php

namespace App\Core\Exception;

class UnauthorizedException extends ExceptionHandler {
    /* Construction de l'exception d'accès non autorisé */
    public function __construct($message = "Accès non autorisé", $code = 401)
    {
        parent::__construct($message, $code);
    }
}
