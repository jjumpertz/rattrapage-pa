<?php
namespace App\Core;

class FormBuilder{


    public function __construct(){

    }

    /* Construction du formulaire HTML */
    public static function render($config, $show=true){

        $html = "<form
        method=\"".($config["config"]["method"]??"GET")."\"
        action=\"".($config["config"]["action"]??"")."\"
        class=\"".($config["config"]["class"]??"")."\"
        id=\"".($config["config"]["id"]??"GET")."\"
        >";

        foreach ($config["inputs"] as $name => $configInput){
            $html .="<label for=\"".($configInput["id"]??$name)."\">".($configInput["label"]??"")."</label><br>";

            if ($configInput["type"] == "select") {
                $html .= "<select name='".$name."'>";

                foreach ($configInput["choix"] as $choix => $nom) {
                    $html .= "<option value='".$choix."'>".$nom."</option>";
                }

                $html .= "</select><br>";
            } elseif ($configInput["type"] == "textarea") {
                $html .= "<textarea id='".($configInput["id"]??$name)."' 
                        name='".($name)."' 
                        rows='".($configInput["rows"]??1)."' 
                        cols='".($configInput["cols"]??5)."'
                        class='".($configInput["class"]??"")."'
                        id='".($configInput["id"]??$name)."'></textarea>";
            } else {
                $html .="<input
                        type=\"".($configInput["type"]??"text")."\"
                        name= \"".$name."\"
                        placeholder=\"".($configInput["placeholder"]??"")."\"
                        class=\"".($configInput["class"]??"")."\"
                        id=\"".($configInput["id"]??$name)."\"
                        value=\"".($configInput["value"]??"")."\"
                        \"".(!empty($configInput["required"])?"required='required'":"")."\"
                        ><br>";
            }

        }


        $html .="<input type=\"submit\" value=\"".($config["config"]["submit"]??"Valider")."\" class=\"button button-success button-hover as-center\" >";
        $html .="</form>";

        if ($show){
            echo $html;
        }else{
            return $html;
        }
    }//render
}