<?php

namespace App\Core;

use App\Core\PHPMailer\Exception;
use App\Core\PHPMailer\PHPMailer;
use App\Core\PhpMailer\SMTP;

class Mails
{
    private $Host;
    private $Port;
    private $SMTPSecure;
    private $Username;
    private $Password;


    public function sendMail($to, $from, $name, $subject, $msg, $alt = null)
    {
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->Host = MAIL_HOST;
        $mail->Port = MAIL_PORT;
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
        $mail->SMTPAuth = true;
        $mail->Username = MAIL_USER;
        $mail->Password = MAIL_PWD;

        $mail->CharSet = 'UTF-8';
        $mail->Encoding = 'base64';

        $mail->setFrom($from);
        $mail->addAddress($to);

        $mail->Subject = $subject;
        $mail->msgHTML($msg);
        $mail->AltBody = $alt ?? $msg;

        $mail->send();

    }
}
?>