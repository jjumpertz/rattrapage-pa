<?php

namespace App\Core;

class Router{

	private $slug;
	private $action;
	private $controller;
	private $routePath = "routes.yml";
	private $listOfRoutes = [];
	private $listOfSlugs = [];
	private $params= [];

	/*	
		- On passe le slug en attribut
		- Execution de la methode loadYaml
		- Vérifie si le slug existe dans nos routes -> SINON appel la methode exception4040
		- call setController et setAction
	*/
	public function __construct($slug = ""){
	    $uriParams = explode("?", $_SERVER["REQUEST_URI"], 2);
		$this->slug = $uriParams[0];
		if (isset($uriParams[1])) {
		    $this->params = $this->initParams($uriParams[1]);
        }
		$this->loadYaml();

		if(empty($this->listOfRoutes[$this->slug])) $this->exception404();

		$this->setController($this->listOfRoutes[$this->slug]["controller"]);
		$this->setAction($this->listOfRoutes[$this->slug]["action"]);
	}//__construct


	/*
		$this->routePath = "routes.yml";
		- On transforme le YAML en array que l'on stock dans listOfRoutes
		- On parcours toutes les routes
			- Si il n'y a pas de controller ou pas d'action -> die()
			- Sinon on alimente un nouveau tableau qui aura pour clé le controller et l'action
	*/
	public function loadYaml(){
		$this->listOfRoutes = yaml_parse_file($this->routePath);
		foreach ($this->listOfRoutes as $slug=>$route) {
			if(empty($route["controller"]) || empty($route["action"]))
				die("Parse YAML ERROR");
			$this->listOfSlugs[$route["controller"]][$route["action"]] = $slug;
		}
	}//loadYaml

    public static function currentSlug($withoutSlash = false): string
    {
        $slug = $_SERVER["REQUEST_URI"];
        //Suppression des GET dans l'url
        $slugExploded = explode("?", $slug);
        return ($withoutSlash ? trim(strtolower($slugExploded[0]), '/') : strtolower($slugExploded[0]));
    }//currentSlug

    /* Récupérer l'URI selon le controller et l'action renseignés */
	public function getSlug($controller, $action): string
    {
		return $this->listOfSlugs[$controller][$action];
	}//getSlug

    /* Utilisation de la fonction si dessus */
	public static function getRoute($controller, $action){
        $slug = mb_strtolower($_SERVER["REQUEST_URI"]);
        $route = new Router($slug);
        return $route->getSlug($controller, $action);
    }//getRoute

    /* Etablir le controller  */
	public function setController($controller){
		$this->controller = ucfirst($controller);
	}//setController

    /* Etablir l'action */
	public function setAction($action){
		$this->action = $action."Action";
	}//setAction

    /* Récupérer le controller */
	public function getController(){
		return $this->controller;
	}//getController

    /* Récupérer l'action */
	public function getAction(){
		return $this->action;
	}//getAction

    /* Lancer une erreur 404 */
	public function exception404(){
	    die("Erreur 404");

	}//exception404

    /* Récupérer les paramètres passés dans l'URL */
	private function initParams($params) {
        $paramsReturn = [];
	    $paramList = explode("&", $params, 2);

	    foreach ($paramList as $param) {
            $data = explode("=", $param);
            if(isset($data[1])) $paramsReturn[$data[0]] =  $data[1];
        }

	    return $paramsReturn;
    }//initParams

}