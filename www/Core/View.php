<?php

namespace App\Core;


class View
{

	private $template; // front ou back
	private $view; // home, 404, user, users, ....
	private $data = [];


    /* Renseigner le template et la vue que l'on veut afficher */
	public function __construct($view, $template = "front"){
		$this->setTemplate($template);
		$this->setView($view);
	}//__construct

    /* Instancier le template si ce dernier existe */
	public function setTemplate($template){
		if(file_exists("Views/templates/".$template.".tpl.php")){
			$this->template = "Views/templates/".$template.".tpl.php";
		}else{
			die("Le template n'existe pas");
		}
	}//setTemplate

    /* Instancier la vue si cette dernière existe */
	public function setView($view){
		if(file_exists("Views/".$view.".view.php")){
			$this->view = "Views/".$view.".view.php";
		}else{
			die("La vue n'existe pas");
		}
	}//setView

    /* Assigner des valeurs à la vue */
	public function assign($key, $value){
		$this->data[$key]=$value;
	}//assign


    /* Afficher le template et la vue */
	public function __destruct(){

		extract($this->data);
		include $this->template;
	}

}
