<?php

namespace App\Models;

use App\Core\Database;
use App\Core\Router;

class Categories extends Database
{
    protected $id = null;
    protected $category;
    protected $user;

    public function __construct()
    {
        parent::__construct();
    }

    public function setId($id)
    {
        $this->id = htmlentities($id);
    }

    public function getId()
    {
        return $this->id;
    }

    public function setCategory($category)
    {
        $this->category = htmlentities(ucwords(strtolower(trim($category))));
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setUser($user)
    {
        $this->user = htmlentities($user);
    }

    public function getUser()
    {
        return $this->user;
    }

    //FIXME: Utile plus-tard pour Yanis ?
    public static function selectCategory(array $categories): array
    {
        $arr = [];
        foreach ($categories as $category){
            $arr[] = ['label' => $category['category'], 'value' => $category['id']];
        }
        return $arr;
    }

    /* Formulaire de création d'une catégorie */
    public function formBuilderCategory()
    {
        return [
            "config" => [
                "method" => "POST",
                "action" => "",
                "class" => "forms",
                "id" => "form_category",
                "submit" => "Enregistrer",
            ],
            "inputs" => [

                "category" => [
                    "type" => "text",
                    "placeholder" => "Exemple : disney",
                    "label" => "Catégorie :",
                    "required" => true,
                    "class" => "input",
                    "maxLength" => 320,
                    "error" => "Vous devez renseigner un nom de catégorie."
                ]
            ]
        ];
    }//formBuilderCategory

    /* Formulaire de modification des informations de la catégorie */
    public function formBuilderUpdate()
    {
        return [

            "config" => [
                "method" => "POST",
                "action" => "",
                "class" => "form_control",
                "id" => "form_update",
                "submit" => "Enregistrer",
            ],
            "inputs" => [

                "id" => [
                    "value" => $_REQUEST['id'],
                    "type" => "hidden",
                ],
                "category" => [
                    "type" => "text",
                    "placeholder" => "Exemple : disney",
                    "label" => "Catégorie",
                    "required" => true,
                    "class" => "form_input",
                    "maxLength" => 320,
                    "error" => "Vous devez renseigner un nom de catégorie."
                ]
            ]
        ];
    }//formBuilderUpdate


}
