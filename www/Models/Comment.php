<?php

namespace App\Models;

use App\Core\Database;

class Comment extends Database
{

    private $id = null;
    protected $text;
    protected $date;
    protected $signaled;
    protected $user;
    protected $playlist = null;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = htmlentities($id);
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
        $this->text = htmlentities($text);
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed date
     */
    public function setDate($date)
    {
        $this->lien = htmlentities($date);
    }

    /**
     * @return mixed
     */
    public function getSignaled()
    {
        return $this->signaled;
    }

    /**
     * @param mixed $signaled
     */
    public function setSignaled($signaled)
    {
        $this->signaled = htmlentities($signaled);
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = htmlentities($user);
    }

    /**
     * @return mixed
     */
    public function getPlaylist()
    {
        return $this->playlist;
    }

    /**
     * @param mixed $playlist
     */
    public function setPlaylist($playlist)
    {
        $this->playlist = htmlentities($playlist);
    }

    public function getComments() {
        return $this->getAllData();
    }

    /* Formulaire pour poster un commentaire */
    public static function formBuilderPost() {
        return [

            "config" => [
                "method" => "POST",
                "action" => "/post-comment",
                "class" => "forms",
                "id" => "form_register",
                "submit" => "Poster",
            ],
            "inputs" => [
                "comment" => [
                    "type" => "textarea",
                    "placeholder" => "Partagez votre avis !",
                    "required" => true,
                    "class" => "textarea",
                    "rows" => "5",
                    "cols" => "1",
                    "minLength" => 2,
                    "maxLength" => 320,
                    "error" => "Le commentaire doit faire entre 2 et 320 caractères."
                ]
            ]
        ];
    }//formBuilderPost
}