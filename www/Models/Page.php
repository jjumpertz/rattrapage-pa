<?php

namespace App\Models;

use App\Core\Database;

class Page extends Database
{

    private $id = null;
    protected $title;
    protected $lien;
    protected $content;
    protected $user;
    protected $status;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = htmlentities($id);
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = htmlentities($title);
    }

    /**
     * @return mixed
     */
    public function getLien()
    {
        return $this->lien;
    }

    /**
     * @param mixed $lien
     */
    public function setLien($lien)
    {
        $this->lien = htmlentities($lien);
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = htmlentities($content, ENT_HTML5, 'UTF-8');
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = htmlentities($user);
    }

    /**
     * @return mixed
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * @param $status
     */
    public function setStatus($status) {
        $this->status = htmlentities($status);
    }

    public function showPages() {
        return $this->getAllData();
    }

    /* Formulaire pour créer une page */
    public static function formBuilderAdd() {
        return [

            "config" => [
                "method" => "POST",
                "action" => "/ajouter-page",
                "class" => "forms",
                "id" => "form_register",
                "submit" => "Ajouter",
            ],
            "inputs" => [

                "title" => [
                    "type" => "text",
                    "placeholder" => "Nom de la page",
                    "label" => "Titre : ",
                    "required" => true,
                    "class" => "input",
                    "minLength" => 2,
                    "maxLength" => 320,
                    "error" => "Le titre doit faire entre 2 et 320 caractères."
                ],

                "url" => [
                    "type" => "text",
                    "placeholder" => "/votre-URL",
                    "label" => "URL : ",
                    "required" => true,
                    "class" => "input",
                ]
            ]
        ];
    }//formBuilderAdd
}