<?php

namespace App\Models;

use App\Core\Database;
use App\Core\Router;
use App\Core\View;
use App\Models\User as UserModel;

class User extends Database
{

    protected $id = null;
    protected $pseudo;
    protected $email;
    protected $emailkey = null;
    protected $emailConfirmed = false;
    protected $pwd;
    protected $role = 0;
    protected $status = 1;
    protected $isDeleted = 0;
    protected $token;

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {
        $this->token = htmlentities($token);
    }

    public function __construct()
    {

        parent::__construct();
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = htmlentities($id);
    }


    /**
     * @return mixed
     */
    public function getPseudo()
    {
        return $this->pseudo;
    }

    /**
     * @param mixed $pseudo
     */
    public function setPseudo($pseudo)
    {
        $this->pseudo = htmlentities(ucwords(strtolower(trim($pseudo))));
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = htmlentities($email);
    }

    /**
     * @return mixed
     */
    public function getEmailKey()
    {
        return $this->emailkey;
    }

    /**
     * @param mixed $emailKey
     */
    public function setEmailkey($key)
    {
        $this->email = htmlentities($key);
    }

    public function isEmailConfirmed()
    {
        return $this->emailConfirmed;
    }

    public function setEmailConfirmed($emailConfirmed)
    {
        $this->emailConfirmed = $emailConfirmed;
    }

    /**
     * @return mixed
     */
    public function getPwd()
    {
        return $this->pwd;
    }

    /**
     * @param mixed $pwd
     */
    public function setPwd($pwd)
    {
        $this->pwd = htmlentities($pwd);
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $pseudo
     */
    public function setCountry($country)
    {
        $this->country = htmlentities(ucwords(strtolower(trim($country))));
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role)
    {
        $this->role = htmlentities($role);
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = htmlentities($status);
    }

    /**
     * @return mixed
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param mixed $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = htmlentities($isDeleted);
    }

    /**
     * @return created_at
     */
    public function getCreated_at()
    {
        return $this->created_at;
    }

    public function generateKeyEmail()
    {
        $length = 15;
        $key = "";
        for ($i = 1; $i < $length; $i++) {
            $key .= mt_rand(0, 9);
        }
        $this->emailkey = $key;
    }

    public function getGeneratedKeyEmail()
    {
        $length = 15;
        $key = "";
        for ($i = 1; $i < $length; $i++) {
            $key .= mt_rand(0, 9);
        }
        return $key;
    }

    /* Formulaire de connexion */
    public function formBuilderLogin()
    {
        return [
            "config" => [
                "method" => "POST",
                "action" => "",
                "class" => "forms",
                "id" => "form_login",
                "submit" => "se connecter",
            ],
            "inputs" => [
                "email" => [
                    "type" => "email",
                    "placeholder" => " Adresse email",
                    "label" => "Adresse email",
                    "required" => true,
                    "class" => "input",
                    "minLength" => 8,
                    "maxLength" => 320,
                    "error" => "Votre email doit faire entre 8 et 320 caractères."
                ],
                "pwd" => [
                    "type" => "password",
                    "placeholder" => " Mot de passe",
                    "label" => " Mot de passe",
                    "required" => true,
                    "class" => "input",
                    "minLength" => 8,
                    "error" => "Votre mot de passe doit faire au moins 8 caractères."
                ]
            ]
        ];
    }//formBuilderLogin

    /* Formulaire de réinitialisation du mot de passe */
    public function formBuilderResetPassword()
    {
        return [
            "config" => [
                "method" => "POST",
                "action" => "",
                "class" => "forms",
                "id" => "form_login",
                "submit" => "Envoyer",
            ],
            "inputs" => [
                "email" => [
                    "type" => "email",
                    "placeholder" => " Adresse email",
                    "label" => "Renseigner l'adresse mail du compte",
                    "required" => true,
                    "class" => "input",
                    "minLength" => 8,
                    "maxLength" => 320,
                    "error" => "Votre email doit faire entre 8 et 320 caractères."
                ]
            ]
        ];
    }//formBuilderResetPassword

    /* Formulaire de création de compte */
    public function formBuilderRegister()
    {
        return [

            "config" => [
                "method" => "POST",
                "action" => "",
                "class" => "forms",
                "id" => "form_register",
                "submit" => "S'inscrire",
            ],
            "inputs" => [

                "pseudo" => [
                    "type" => "text",
                    "placeholder" => "Pseudo",
                    "label" => "Votre Pseudo",
                    "required" => true,
                    "class" => "input",
                    "minLength" => 2,
                    "maxLength" => 320,
                    "error" => "Votre pseudo doit faire entre 8 et 320 caractères."
                ],

                "email" => [
                    "type" => "email",
                    "placeholder" => "Adresse email",
                    "label" => "Adresse Email",
                    "required" => true,
                    "class" => "input",
                    "minLength" => 8,
                    "maxLength" => 320,
                    "error" => "Votre email doit faire entre 8 et 320 caractères."
                ],

                "pwd" => [
                    "type" => "password",
                    "placeholder" => "Mot de passe",
                    "label" => "Mot de passe",
                    "required" => true,
                    "class" => "input",
                    "minLength" => 8,
                    "error" => "Votre mot de passe doit faire au moins 8 caractères."
                ],

                "pwdConfirm" => [
                    "type" => "password",
                    "placeholder" => "Confirmer",
                    "label" => "Confirmer",
                    "required" => true,
                    "class" => "input",
                    "confirm" => "pwd",
                    "error" => "Votre mot de passe de confirmation ne correspond pas."
                ]
            ]
        ];
    }//formBuilderRegister

    /* Formulaire de modification du compte */
    public function formBuilderUpdate()
    {
        $user = new User();
        $user = $user->getOneByIdSession();
        return [

            "config" => [
                "method" => "POST",
                "action" => Router::getRoute("User", "updateProfile"),
                "class" => "forms",
                "id" => "submitProfil",
                "submit" => "Enregistrer les changements",
            ],
            "inputs" => [
                "pseudo" => [
                    "type" => "text",
                    "placeholder" => "Pseudo",
                    "label" => "Nouveau Pseudo : ",
                    "class" => "input",
                    "value" => $user->getPseudo(),
                    "minLength" => 2,
                    "maxLength" => 320,
                    "error" => "Votre pseudo doit faire entre 8 et 320 caractères."
                ],
                "email" => [
                    "type" => "email",
                    "placeholder" => "Exemple : nom@gmail.com",
                    "label" => "Nouvel Email : ",
                    "class" => "input",
                    "value" => $user->getEmail(),
                    "minLength" => 8,
                    "maxLength" => 320,
                    "error" => "Votre email doit faire entre 8 et 320 caractères."
                ]

            ]
        ];
    }//formBuilderUpdate

    /* Formulaire de modification du mot de passe du compte */
    public function formBuilderUpdatePassword()
    {
        return [

            "config" => [
                "method" => "POST",
                "action" => Router::getRoute("User", "updatePassword"),
                "class" => "forms",
                "id" => "submitPwd",
                "submit" => "Modifier le mot de passe",
            ],
            "inputs" => [
                "pwd" => [
                    "type" => "password",
                    "label" => "Nouveau mot de passe : ",
                    "class" => "input",
                    "minLength" => 8,
                    "error" => "Votre mot de passe doit faire au moins 8 caractères."
                ],
                "pwdConfirm" => [
                    "type" => "password",
                    "label" => "Confirmation du nouveau mot de passe : ",
                    "class" => "input",
                    "minLength" => 8,
                    "error" => "Votre mot de passe de confirmation est incorrect."
                ]

            ]
        ];
    }//formBuilderUpdate
}
