<?php
use App\Core\Router;
?>
<p class ="alert alert-error col-12-lap col-12-tab col-12-mob txt-center">Vous devez être connecté pour accéder à cette page !<br>
Vous allez être redirigé.</p>
<?php
    header("refresh:3; url=".Router::getRoute("User","login"));
?>
