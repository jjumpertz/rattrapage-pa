<?php

if (isset($page) && isset($createur)) {
    echo "Page créée par " . $createur->getPseudo();
    echo html_entity_decode($page->getContent(), ENT_HTML5, 'UTF-8');
}