<p class="hidden alert alert-error"></p>
<p id="tinyMCEQuote" class="hidden quotes col-3-lap txt-h-center"></p>
<?php

use App\Core\Router;

if (isset($page)) {
?>
    <form method="post">
        <input type="hidden" name="idPage" value="<?= $page->getId() ?>">
        <div>
            <label for="title">Titre : </label>
            <input type="text" name="title" value="<?= $page->getTitle() ?? "" ?>">
            <label for="URL">URL : </label>
            <input type="text" name="URL" value="<?= $page->getLien() ?? "" ?>">
        </div>
        <div class="">
            <textarea id="inte_tinyMCE" rows="35"><?= $page->getContent() ?? "" ?></textarea>
        </div>
    </form>
    <button class="button button-info" onclick="save()">Sauvegarder</button>
    <form action="<?= Router::getRoute("Pages", "delete"); ?>" method="POST">
        <input type="hidden" name="id" value="<?= $page->getId() ?>">
        <input type="submit" class="button button-error" value="Supprimer">
    </form>
<?php
    if ($page->getStatus() == "0") {
?>
        <button onclick="publier()">Publier</button>
<?php
    }
}
?>