<?php

use App\Core\Router;
if (isset($pages)) {
    foreach ($pages as $page) { ?>
        <form action="<?= Router::getRoute("Pages", "show"); ?>" method="GET">
            <p><?= $page->getTitle() ?></p>
            <input type="hidden" name="id" value="<?= $page->getId() ?>">
            <input type="submit" value="Aller sur la page">
        </form>
        <?php
    }
} ?>
