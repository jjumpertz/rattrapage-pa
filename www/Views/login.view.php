<?php if (isset($errors)): ?>
    <?php foreach ($errors as $error): ?>
        <p class="alert alert-error col-12-mob"><?= $error ?></p>
    <?php endforeach; ?>
<?php endif; ?>

<div class="forms bg-white col-start-3 col-end-11">
    <?= App\Core\FormBuilder::render($formLogin); ?>
    <a href="/reset-pwd" class="txt-h-center link-visit">Mot de passe oublié</a>
</div>
