<?php

use App\Core\Router;

?>
<h2 class="col-12-lap">Liste des pages</h2>
<?php
if(isset($messages)) {
    foreach ($messages as $message)
    {
        echo $message."<br/>";
    }
}

if (isset($formAddPage)) {
    App\Core\FormBuilder::render($formAddPage);
}

if (isset($pages)) {
    foreach ($pages as $page) { ?>
        <div class="col-12-lap card row">
            <p><?= html_entity_decode($page->getTitle()); ?></p>
            <p>Fait par : <?= html_entity_decode($page->getUser()); ?></p>
            <p>Statut : <?= html_entity_decode($page->getStatus()); ?></p>
            <form class="txt-center col-start-10 col-end-11" action="<?= Router::getRoute("Pages", "edit"); ?>" method="POST">
                <input type="hidden" name="id" value="<?= $page->getId() ?>">
                <input type="submit" value="Modifier" class="button button-info">
            </form>
            <form action="<?= Router::getRoute("Pages", "show"); ?>" method="GET">
                <input type="hidden" name="id" value="<?= $page->getId() ?>">
                <input type="submit" value="Aller sur la page via ID" class="button button-warning">
            </form>
            <form action="<?= Router::getRoute("Pages", "show"); ?>" method="GET">
                <input type="hidden" name="url" value="<?= $page->getLien() ?>">
                <input type="submit" value="Aller sur la page via URL" class="button button-warning">
            </form>
            <form action="<?= Router::getRoute("Pages", "delete"); ?>" method="POST">
                <input type="hidden" name="id" value="<?= $page->getId()?>">
                <input type="submit" class="button button-error" value="Supprimer">
            </form>
        </div>

    <?php }
}
