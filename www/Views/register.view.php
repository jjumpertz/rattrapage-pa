<?php if (isset($errors)): ?>
    <?php foreach ($errors as $error): ?>
        <p class="alert alert-error col-12-mob"><?= $error ?></p>
    <?php endforeach; ?>
<?php endif; ?>
<div class="forms bg-white col-start-3 col-end-11">
    <?php App\Core\FormBuilder::render($form); ?>
</div>
