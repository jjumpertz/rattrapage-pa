<?php

use App\Core\Router;

$slug = mb_strtolower($_SERVER["REQUEST_URI"]);
$route = new Router($slug);
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Rattrapage</title>
    <meta name="description" content="description de la page de front">
    <script src="/public/js/jquery-3.6.0.min.js" type="text/javascript"></script>
    <script src="https://cdn.tiny.cloud/1/rmynjq4ndkls4sjh9lk17nho4qcfvvk56y6q3s6mz7n9kwgi/tinymce/5/tinymce.min.js"
            referrerpolicy="origin"></script>
    <script src="../../public/js/pageBuilder.js" type="text/javascript"></script>
</head>
<body>
    <ul class="menu">
        <li><a href="<?= Router::getRoute('Main', 'default') ?>">Accueil</a></li>
        <li><a href="<?= Router::getRoute('User', 'showAll') ?>">Utilisateurs</a></li>


        <?php if (isset($_SESSION['id']) && $_SESSION['id'] != "") { ?>
                <li><a href="<?= Router::getRoute('Pages', 'default') ?>">Pages</a></li>
            <li><a href="<?= Router::getRoute('User', 'updateUser') ?>">Modifier mon profil</a></li></div>
            <li><a href="<?= Router::getRoute('User', 'deconnexion') ?>">Se deconnecter</a></li>
        <?php } else { ?>

            <li><a href="<?= Router::getRoute('User', 'login') ?>">Se Connecter</a></li>
            <li><a href="<?= Router::getRoute('User', 'register') ?>">S'inscrire</a></li>

        <?php } ?>
    </ul>

<main>
        <div>
            <!-- afficher la vue -->
            <?php include $this->view ?>
        </div>

</main>
</body>
</html>
