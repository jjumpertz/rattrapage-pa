
<?php
use App\Core\Router;

if (isset($user)) {
    ?>
    <div class="col-12-lap">
        <div><?= "Pseudo : " . $user->getPseudo() ?></div>
        <div><?= "Email : " . $user->getEmail() ?></div>
        <div><h4><?= "Date inscription : " . date("d/m/y", strtotime($user->getCreated_at())) ?></h4></div>
        <form action="<?= Router::getRoute('User', 'updateProfil'); ?>" method="GET">
            <input type="hidden" name="id_user" value="<?= $user->getId() ?>">
            <input type="submit" value="Modifier votre profil" class="button button-info">
        </form>

        <form action="<?= Router::getRoute('User', 'removeUser') ?>" method="POST">
            <input type="hidden" name="id" value="<?= $user->getId() ?>">
            <button type="submit" name="formdeleteuser" class="button button-error">Supprimer l'utilisateur</button>
        </form>
    </div>

    <?php
} ?>
