<h2>Liste des utilisateurs</h2>

<?php
use App\Core\Router;

if (isset($users)) {
    foreach ($users as $user): ?>

        <div class="user">
            <form action="<?= Router::getRoute('User', 'show') ?>" method="POST">
                <?= $user->getPseudo() ?>
                <input type="hidden" name="id" value="<?= $user->getId() ?>">
                <input type="submit" value="Voir Profil">
            </form>

            <form action="<?= Router::getRoute('User', 'removeUser') ?>" method="POST">
                <input type="hidden" name="id" value="<?= $user->getId() ?>">
                <input type="submit" value="Supprimer Profil">
            </form>
        </div>
    <?php endforeach;
} ?>
