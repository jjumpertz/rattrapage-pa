
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Table `jclm_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jclm_user` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `pseudo` VARCHAR(45) NULL,
    `pwd` VARCHAR(255) NULL,
    `email` VARCHAR(320) NULL,
    `game` INT NULL,
    `status` INT NULL DEFAULT 1,
    `role` INT NULL DEFAULT 0,
    `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    `isDeleted` TINYINT NULL DEFAULT 0,
    `isPlaying` TINYINT NULL DEFAULT 0,
    PRIMARY KEY (`id`));



-- -----------------------------------------------------
-- Table `jclm_Music`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jclm_Music` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `duration` DATE NULL,
    `name` VARCHAR(45) NULL,
    `artist` VARCHAR(45) NULL,
    `gender` VARCHAR(45) NULL,
    `user` INT NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_jclm_Music_jclm_user1_idx` (`user` ASC),
    CONSTRAINT `fk_jclm_Music_jclm_user1`
    FOREIGN KEY (`user`)
    REFERENCES `jclm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);



-- -----------------------------------------------------
-- Table `jclm_category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jclm_categories` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `category` VARCHAR(45) CHARACTER SET 'big5' NULL,
    `duration` DATE NULL,
    `user` INT NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_jclm_category_jclm_user1_idx` (`user` ASC),
    CONSTRAINT `fk_jclm_category_jclm_user1`
    FOREIGN KEY (`user`)
    REFERENCES `jclm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `jclm_playlist`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jclm_playlist` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45) NULL,
    `duration` DATE NULL,
    `nbUtilisation` INT NULL,
    `category` INT NOT NULL,
    `user` INT NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_jclm_playlist_jclm_category1_idx` (`category` ASC),
    INDEX `fk_jclm_playlist_jclm_user1_idx` (`user` ASC),
    CONSTRAINT `fk_jclm_playlist_jclm_category1`
    FOREIGN KEY (`category`)
    REFERENCES `jclm_categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    CONSTRAINT `fk_jclm_playlist_jclm_user1`
    FOREIGN KEY (`user`)
    REFERENCES `jclm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);



-- -----------------------------------------------------
-- Table `jclm_game`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jclm_game` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `private` TINYINT NULL,
    `playlist` INT NOT NULL,
    `category` INT NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_jclm_game_jclm_playlist1_idx` (`playlist` ASC),
    INDEX `fk_jclm_game_jclm_category1_idx` (`category` ASC),
    CONSTRAINT `fk_jclm_game_jclm_playlist1`
    FOREIGN KEY (`playlist`)
    REFERENCES `jclm_playlist` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    CONSTRAINT `fk_jclm_game_jclm_category1`
    FOREIGN KEY (`category`)
    REFERENCES `jclm_categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);



-- -----------------------------------------------------
-- Table `jclm_chat`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jclm_chat` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `message` VARCHAR(45) NULL,
    `date` DATE NULL,
    `game` INT NOT NULL,
    `user` INT NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_jclm_chat_jclm_game1_idx` (`game` ASC),
    INDEX `fk_jclm_chat_jclm_user1_idx` (`user` ASC),
    CONSTRAINT `fk_jclm_chat_jclm_game1`
    FOREIGN KEY (`game`)
    REFERENCES `jclm_game` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    CONSTRAINT `fk_jclm_chat_jclm_user1`
    FOREIGN KEY (`user`)
    REFERENCES `jclm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);



-- -----------------------------------------------------
-- Table `jclm_commentary`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jclm_comment` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `text` VARCHAR(45) NULL,
    `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    `signaled` INT NULL DEFAULT 0,
    `user` INT NOT NULL,
    `playlist` INT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_jclm_commentary_jclm_user1_idx` (`user` ASC),
    INDEX `fk_jclm_commentary_jclm_playlist1_idx` (`playlist` ASC),
    CONSTRAINT `fk_jclm_commentary_jclm_user1`
    FOREIGN KEY (`user`)
    REFERENCES `jclm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    CONSTRAINT `fk_jclm_commentary_jclm_playlist1`
    FOREIGN KEY (`playlist`)
    REFERENCES `jclm_playlist` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);



-- -----------------------------------------------------
-- Table `jclm_image`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jclm_image` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `src` VARCHAR(200) NULL,
    `user` INT NOT NULL,
    `category` INT NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_jclm_image_jclm_user1_idx` (`user` ASC),
    INDEX `fk_jclm_image_jclm_category1_idx` (`category` ASC),
    CONSTRAINT `fk_jclm_image_jclm_user1`
    FOREIGN KEY (`user`)
    REFERENCES `jclm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    CONSTRAINT `fk_jclm_image_jclm_category1`
    FOREIGN KEY (`category`)
    REFERENCES `jclm_categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `jclm_participé`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jclm_participe` (
    `game` INT NOT NULL,
    `user` INT NOT NULL,
    `ptp_score` DOUBLE NULL,
    INDEX `fk_jclm_participé_jclm_game1_idx` (`game` ASC),
    INDEX `fk_jclm_participé_jclm_user1_idx` (`user` ASC),
    PRIMARY KEY (`game`, `user`),
    CONSTRAINT `fk_jclm_participé_jclm_game1`
    FOREIGN KEY (`game`)
    REFERENCES `jclm_game` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    CONSTRAINT `fk_jclm_participé_jclm_user1`
    FOREIGN KEY (`user`)
    REFERENCES `jclm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `jclm_incorpore`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jclm_incorpore` (
    `playlist` INT NOT NULL,
    `music` INT NOT NULL,
    INDEX `fk_jclm_incorpore_jclm_playlist1_idx` (`playlist` ASC),
    INDEX `fk_jclm_incorpore_jclm_Music1_idx` (`music` ASC),
    PRIMARY KEY (`playlist`, `music`),
    CONSTRAINT `fk_jclm_incorpore_jclm_playlist1`
    FOREIGN KEY (`playlist`)
    REFERENCES `jclm_playlist` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    CONSTRAINT `fk_jclm_incorpore_jclm_Music1`
    FOREIGN KEY (`music`)
    REFERENCES `jclm_Music` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `jclm_theme`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jclm_theme` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `nom` VARCHAR(45) NULL,
    `description` VARCHAR(45) NULL,
    `lien` VARCHAR(45) NULL,
    PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table `jclm_applique`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jclm_applique` (
    `user` INT NOT NULL,
    `theme` INT NOT NULL,
    INDEX `fk_jclm_applique_jclm_user1_idx` (`user` ASC),
    INDEX `fk_jclm_applique_jclm_theme1_idx` (`theme` ASC),
    PRIMARY KEY (`user`, `theme`),
    CONSTRAINT `fk_jclm_applique_jclm_user1`
    FOREIGN KEY (`user`)
    REFERENCES `jclm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    CONSTRAINT `fk_jclm_applique_jclm_theme1`
    FOREIGN KEY (`theme`)
    REFERENCES `jclm_theme` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `jclm_page`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jclm_page` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(50) NULL,
    `theme` INT NOT NULL DEFAULT 1,
    `user` INT NOT NULL,
    `lien` VARCHAR(45) NULL,
    `content` LONGTEXT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_jclm_page_jclm_theme1_idx` (`theme` ASC),
    INDEX `fk_jclm_page_jclm_user1_idx` (`user` ASC),
    CONSTRAINT `fk_jclm_page_jclm_theme1`
    FOREIGN KEY (`theme`)
    REFERENCES `jclm_theme` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    CONSTRAINT `fk_jclm_page_jclm_user1`
    FOREIGN KEY (`user`)
    REFERENCES `jclm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `jclm_contient`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jclm_contient` (
    `page` INT NOT NULL,
    `image` INT NOT NULL,
    PRIMARY KEY (`page`, `image`),
    INDEX `fk_jclm_contient_jclm_image1_idx` (`image` ASC),
    CONSTRAINT `fk_jclm_contient_jclm_page1`
    FOREIGN KEY (`page`)
    REFERENCES `jclm_page` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    CONSTRAINT `fk_jclm_contient_jclm_image1`
    FOREIGN KEY (`image`)
    REFERENCES `jclm_image` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `table1`
-- -----------------------------------------------------

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
