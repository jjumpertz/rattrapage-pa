
tinymce.init({
    selector: '#inte_tinyMCE',
    plugins: 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    toolbar: 'a11ycheck addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table link',
    toolbar_mode: 'floating',
    tinycomments_mode: 'embedded',
    tinycomments_author: 'Author name'
});

function save() {
    $("#tinyMCEQuote").addClass("quotes-info");
    $("#tinyMCEQuote").html("En cours de sauvegarde...");
    $("#tinyMCEQuote").removeClass("hidden");

    idPage = $("input[name='idPage']").val();
    URLPage = $("input[name='URL']").val();
    titlePage = $("input[name='title']").val();
    contentPage = tinymce.activeEditor.getContent();

    data = {id: idPage, title: titlePage, url: URLPage, content: contentPage};
    data = JSON.stringify(data);

    $.ajax({
        url: '/sauver-page',
        type: 'POST',
        data: {id: idPage,
            title: titlePage,
            url: URLPage,
            content: contentPage},
        dataType: 'json',
        success: function (data, status, xhr) {
            if (data.reponse === 200) {
                $("#tinyMCEQuote").removeClass("quotes-info");
                $("#tinyMCEQuote").addClass("quotes-success");
                $("#tinyMCEQuote").html(data.msg);
            } else {
                $("#tinyMCEQuote").removeClass("quotes-info");
                $("#tinyMCEQuote").addClass("quotes-error");
                $("#tinyMCEQuote").html(data.msg);
            }
        },
        error: function (response) {
            console.log(response)
            $("#tinyMCEQuote").removeClass("quotes-info");
            $("#tinyMCEQuote").addClass("quotes-error");
            $("#tinyMCEQuote").html("Erreur lors de la sauvegarde.");
        }
    });
}

function publier() {
    $("#tinyMCEQuote").addClass("quotes-info");
    $("#tinyMCEQuote").html("En cours de publication...");
    $("#tinyMCEQuote").removeClass("hidden");

    idPage = $("input[name='idPage']").val();

    data = {id: idPage};
    data = JSON.stringify(data);

    $.ajax({
        url: '/publier-page',
        type: 'POST',
        data: {id: idPage},
        dataType: 'json',
        success: function (data, status, xhr) {
            if (data.reponse === 200) {
                $("#tinyMCEQuote").removeClass("quotes-info");
                $("#tinyMCEQuote").addClass("quotes-success");
                $("#tinyMCEQuote").html(data.msg);
            } else {
                $("#tinyMCEQuote").removeClass("quotes-info");
                $("#tinyMCEQuote").addClass("quotes-error");
                $("#tinyMCEQuote").html(data.msg);
            }
        },
        error: function (response) {
            console.log(response)
            $("#tinyMCEQuote").removeClass("quotes-info");
            $("#tinyMCEQuote").addClass("quotes-error");
            $("#tinyMCEQuote").html("Erreur lors de la publication.");
        }
    });
}